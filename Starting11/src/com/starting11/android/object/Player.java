package com.starting11.android.object;

import java.io.Serializable;

import com.google.gson.annotations.SerializedName;

public class Player implements Serializable{

	@SerializedName("name")
	public String name;
	
	@SerializedName("number")
	public String number;
	
	@SerializedName("card")
	public String card = "";
	
	@SerializedName("card_value")
	public String card_value = "";
	
	@SerializedName("isReplaced")
	public String isReplace;
	
	@SerializedName("action_time")
	public String action_time;
	
	@SerializedName("goal_type")
	public String goal_type;
	
	@SerializedName("replaced_time")
	public String replaced_time;
	
	public String type;
	
	public boolean isSeperator = false;
}
