package com.starting11.android.ui;

import android.webkit.WebView;

import com.googlecode.androidannotations.annotations.AfterViews;
import com.googlecode.androidannotations.annotations.EActivity;
import com.googlecode.androidannotations.annotations.OptionsItem;
import com.googlecode.androidannotations.annotations.ViewById;
import com.starting11.android.R;
import com.starting11.android.core.BaseActivity;

@EActivity(R.layout.activity_about)
public class About extends BaseActivity{

	@ViewById
	WebView web;

	@AfterViews
	public void calledAfterViewInjection() {

		getSupportActionBar().setTitle("About Us");
		getSupportActionBar().setIcon(R.drawable.ball);

		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setHomeButtonEnabled(true);

		web.loadUrl("file:///android_asset/about.html");
	}

	@OptionsItem(android.R.id.home)
	void home(){
		finish();
	}
}
