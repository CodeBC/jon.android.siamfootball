package com.starting11.android.ui;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.TimeZone;

import org.json.JSONException;

import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.googlecode.androidannotations.annotations.AfterViews;
import com.googlecode.androidannotations.annotations.Bean;
import com.googlecode.androidannotations.annotations.Click;
import com.googlecode.androidannotations.annotations.EActivity;
import com.googlecode.androidannotations.annotations.Extra;
import com.googlecode.androidannotations.annotations.OptionsItem;
import com.googlecode.androidannotations.annotations.OptionsMenu;
import com.googlecode.androidannotations.annotations.ViewById;
import com.googlecode.androidannotations.annotations.res.StringRes;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.starting11.android.R;
import com.starting11.android.Starting11App;
import com.starting11.android.adapter.PlayerAdapter;
import com.starting11.android.core.BaseActivity;
import com.starting11.android.exception.ReturnException;
import com.starting11.android.object.Country;
import com.starting11.android.object.Match;
import com.starting11.android.object.Player;
import com.starting11.android.utils.L;
import com.starting11.android.view.ScorerItemView;
import com.starting11.android.view.ScorerItemView_;

@EActivity(R.layout.activity_match_detail)
@OptionsMenu(R.menu.menu_refresh)
public class MatchDetail extends BaseActivity {

	@Extra("country")
	Country country;

	@Extra("match")
	Match match;

	@ViewById
	TextView vs;

	@ViewById
	TextView station;

	@ViewById
	TextView team1_pen;

	@ViewById
	TextView team2_pen;

	@ViewById
	TextView text_pen;

	@ViewById
	TextView halftime;

	@ViewById
	LinearLayout team_1_scorer;

	@ViewById
	LinearLayout team_2_scorer;

	@ViewById
	TextView referee;

	@ViewById
	TextView current_time;

	@ViewById
	TextView team1_score;

	@ViewById
	TextView team2_score;

	@ViewById
	TextView team_1;

	@ViewById
	TextView team_2;

	@ViewById
	RelativeLayout team1_totoal_redcard;

	@ViewById
	RelativeLayout team2_totoal_redcard;

	@ViewById
	TextView team1_totoal_redcard_txt;

	@ViewById
	TextView team2_totoal_redcard_txt;

	@StringRes(R.string.url_match_detail)
	String url;

	@ViewById
	ImageView loading;

	@ViewById
	RelativeLayout content;

	@ViewById
	ListView list_player;

	@Bean
	PlayerAdapter adapter;

	boolean isLoading;

	int i = 0;

	String match_id, country_id;

	SimpleDateFormat old_df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
	SimpleDateFormat current_df = new SimpleDateFormat("HH:mm");

	@AfterViews
	public void calledAfterViewInjection() {

		getSupportActionBar().setTitle(country.name);
		getSupportActionBar().setIcon(country.icon);

		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setHomeButtonEnabled(true);

		match_id = match.match_id + "";
		country_id = country.id;

		old_df.setTimeZone(TimeZone.getTimeZone("GMT+8"));
		current_df.setTimeZone(TimeZone.getDefault());

		getMatchDetail();
	}

	@Click(R.id.loading)
	@OptionsItem(R.id.action_refresh)
	void OnRefresh() {
		if (!isLoading)
			getMatchDetail();
	}

	private void getMatchDetail() {

		isLoading = true;

		loading.setVisibility(ImageView.VISIBLE);
		content.setVisibility(RelativeLayout.GONE);
		loading.startAnimation(AnimationUtils.loadAnimation(mContext,
				R.anim.rotate_indefinitely));

		if (Starting11App.hasInternet(mContext)) {
			Ion.with(mContext).load(url).setBodyParameter("match_id", match_id)
					.setBodyParameter("country", country_id)
					.setBodyParameter("key", pref.key().get()).asString()
					.setCallback(new FutureCallback<String>() {
						@Override
						public void onCompleted(Exception e, String result) {

							try {
								if (e != null)
									throw e;

								parseMatchDetail(result);

								isLoading = false;

							} catch (Exception ex) {
								ex.printStackTrace();

								isLoading = false;

								Toast.makeText(mContext, "Unable to connect",
										Toast.LENGTH_SHORT).show();

								loading.setVisibility(ImageView.VISIBLE);
								content.setVisibility(ImageView.GONE);
								loading.setAnimation(null);
							}

						}

					});
		} else {

			isLoading = false;

			L.i("No Connection");

			loading.setVisibility(ImageView.VISIBLE);
			content.setVisibility(ImageView.GONE);
			loading.setAnimation(null);

			Toast.makeText(mContext, "No Active Internet Connection",
					Toast.LENGTH_SHORT).show();
		}

	}

	private void parseMatchDetail(String input) throws ReturnException,
			JSONException {
		JsonParser parser = new JsonParser();

		JsonObject jobj_main = (JsonObject) parser.parse(input.trim());

		int status = jobj_main.get("status").getAsInt();

		L.i("Status : " + status);

		Gson gson = new GsonBuilder().create();

		if (status == 2) {

			JsonArray jarr_items = jobj_main.getAsJsonArray("items");

			for (int it = 0; it < jarr_items.size(); it++) {
				JsonObject jobj_items = jarr_items.get(it).getAsJsonObject();

				L.i(jobj_items.toString());

				// match = gson.fromJson(jobj_items, Match.class);

				match = new Match();

				match.country = jobj_items.get("country").getAsString();
				match.team_1 = jobj_items.get("team1").getAsString();
				match.team_2 = jobj_items.get("team2").getAsString();
				match.station = jobj_items.get("stadium").getAsString();
				match.ref = jobj_items.get("ref").getAsString();
				match.team_1_score = jobj_items.get("team1_score").getAsInt();
				match.team_2_score = jobj_items.get("team2_score").getAsInt();
				match.team1_penaltyshoot = jobj_items.get("team1_penaltyshoot")
						.getAsInt();
				match.team2_penaltyshoot = jobj_items.get("team2_penaltyshoot")
						.getAsInt();
				match.isHalfTime = jobj_items.get("isHalfTime").getAsString();
				match.currentTimeStatus = jobj_items.get("currentTimeStatus")
						.getAsString();

				if (jobj_items.get("currentTime").isJsonNull())
					match.current_time = "00:00";
				else
					match.current_time = jobj_items.get("currentTime")
							.getAsString();

				JsonArray jarr_team1_scorer = jobj_items
						.getAsJsonArray("team1_scorer");

				for (int i = 0; i < jarr_team1_scorer.size(); i++) {
					Player player = gson.fromJson(jarr_team1_scorer.get(i),
							Player.class);
					match.team1_scorers.add(player);
				}

				JsonArray jarr_team2_scorer = jobj_items
						.getAsJsonArray("team2_scorer");

				for (int i = 0; i < jarr_team2_scorer.size(); i++) {
					Player player = gson.fromJson(jarr_team2_scorer.get(i),
							Player.class);
					match.team2_scorers.add(player);
				}

				calculateHalfTimeScore();

				JsonArray jarr_team1_miss = jobj_items
						.getAsJsonArray("team1_misspenalty");

				for (int i = 0; i < jarr_team1_miss.size(); i++) {
					Player player = gson.fromJson(jarr_team1_miss.get(i),
							Player.class);
					player.goal_type = "miss";
					match.team1_scorers.add(player);
				}

				JsonArray jarr_team2_miss = jobj_items
						.getAsJsonArray("team2_misspenalty");

				for (int i = 0; i < jarr_team2_miss.size(); i++) {
					Player player = gson.fromJson(jarr_team2_miss.get(i),
							Player.class);
					player.goal_type = "miss";
					match.team2_scorers.add(player);
				}

				// Team 1 Player parse
				JsonObject jobj_1_player = jobj_items
						.getAsJsonObject("team1_players");
				JsonObject jobj_2_player = jobj_items
						.getAsJsonObject("team2_players");

				//
				ArrayList<Player> team1_temp_player = new ArrayList<Player>();
				ArrayList<Player> team2_temp_player = new ArrayList<Player>();

				ArrayList<Player> team1_temp_rs = new ArrayList<Player>();
				ArrayList<Player> team2_temp_rs = new ArrayList<Player>();

				// Parsing DF,GK,MF,ST
				team1_temp_player
						.addAll(parsePlayerByType("GK", jobj_1_player));
				team1_temp_player
						.addAll(parsePlayerByType("DF", jobj_1_player));
				team1_temp_player
						.addAll(parsePlayerByType("MF", jobj_1_player));
				team1_temp_player
						.addAll(parsePlayerByType("ST", jobj_1_player));

				team2_temp_player
						.addAll(parsePlayerByType("GK", jobj_2_player));
				team2_temp_player
						.addAll(parsePlayerByType("DF", jobj_2_player));
				team2_temp_player
						.addAll(parsePlayerByType("MF", jobj_2_player));
				team2_temp_player
						.addAll(parsePlayerByType("ST", jobj_2_player));

				// Re-match it
				// match.team1_players = matchPlayer(team1_temp_player);
				// match.team2_players = matchPlayer(team2_temp_player);

				int added_player = 0;

				if (team1_temp_player.size() > team2_temp_player.size()) {
					added_player = team1_temp_player.size()
							- team2_temp_player.size();

					for (int i = 0; i < added_player; i++) {
						Player p = new Player();
						p.name = "";
						p.number = "";
						p.card = "";
						p.isReplace = "N";
						p.replaced_time = "";
						p.type = "";

						team2_temp_player.add(p);
					}
				} else {
					added_player = team2_temp_player.size()
							- team1_temp_player.size();

					for (int i = 0; i < added_player; i++) {
						Player p = new Player();
						p.name = "";
						p.number = "";
						p.card = "";
						p.isReplace = "N";
						p.replaced_time = "";
						p.type = "";

						team1_temp_player.add(p);
					}
				}

				match.team1_players.addAll(team1_temp_player);
				match.team2_players.addAll(team2_temp_player);

				// Add Seperator
				Player sep = new Player();
				sep.name = " ";
				sep.type = "RS";
				sep.isSeperator = true;

				match.team1_players.add(sep);
				match.team2_players.add(sep);

				// Parse RS
				team1_temp_rs.addAll(parsePlayerByType("RS", jobj_1_player));
				team2_temp_rs.addAll(parsePlayerByType("RS", jobj_2_player));

				// This var hold how much holder needed to add
				int max_rs = 0;

				// Check which team has more RS and update accordingly
				if (team1_temp_rs.size() > team2_temp_rs.size()) {
					max_rs = team1_temp_rs.size() - team2_temp_rs.size();

					for (int i = 0; i < max_rs; i++) {

						Player player = new Player();
						player.name = "----";
						player.number = "";
						player.card = "";
						player.isReplace = "N";
						player.replaced_time = "";
						player.type = "RS";

						team2_temp_rs.add(player);
					}

				} else {

					max_rs = team2_temp_rs.size() - team1_temp_rs.size();

					for (int i = 0; i < max_rs; i++) {

						Player player = new Player();
						player.name = "----";
						player.number = "";
						player.card = "";
						player.isReplace = "N";
						player.replaced_time = "";
						player.type = "RS";

						team1_temp_rs.add(player);
					}
				}

				match.team1_players.addAll(team1_temp_rs);
				match.team2_players.addAll(team2_temp_rs);

			}

			loading.setVisibility(ImageView.GONE);
			loading.setAnimation(null);
			content.setVisibility(RelativeLayout.VISIBLE);

			L.i("Team 1 : " + match.team1_players.size());
			L.i("Team 2 : " + match.team2_players.size());

			setData();

		} else {
			throw new ReturnException("Server Return is not 1");
		}

		isLoading = false;

	}

	private ArrayList<Player> parsePlayerByType(String type, JsonObject team) {

		Gson gson = new GsonBuilder().create();

		ArrayList<Player> types = new ArrayList<Player>();

		JsonArray team_type = team.get(type).getAsJsonArray();

		for (int i = 0; i < team_type.size(); i++) {
			Player player = gson.fromJson(team_type.get(i), Player.class);
			player.type = type;
			types.add(player);
		}

		return types;
	}

	public ArrayList<Player> matchPlayer(ArrayList<Player> player) {
		ArrayList<Player> rs = new ArrayList<Player>();
		ArrayList<Player> temp = new ArrayList<Player>();

		// Seperate RS and other Players
		for (int i = 0; i < player.size(); i++) {
			if (!player.get(i).isSeperator) {
				if (player.get(i).type.equalsIgnoreCase("RS")) {
					rs.add(player.get(i));
				} else if (!player.get(i).type.equalsIgnoreCase("RS")) {
					temp.add(player.get(i));
				}
			}

		}

		// Check Other players size. If less than 11, add holder
		for (int i = 0; i < (11 - temp.size()); i++) {
			Player p = new Player();
			p.name = "";
			p.number = "";
			p.card = "";
			p.isReplace = "N";
			p.replaced_time = "";
			p.type = "";
			temp.add(p);
		}

		temp.addAll(rs);

		return temp;
	}

	private boolean isHalfTime() {

		String temp = match.current_time.replaceAll("\"", "").replace("\\", "")
				.replaceAll("'", "");

		if (temp.contains("+"))
			return true;

		if (Integer.parseInt(temp) > 45)
			return true;

		return false;
	}

	private void calculateHalfTimeScore() {

		int team1 = 0;
		int team2 = 0;

		for (int i = 0; i < match.team1_scorers.size(); i++) {

			String temp = match.team1_scorers.get(i).action_time.replaceAll(
					"'", "").replaceAll(":", "");

			if (temp.contains("+")) {
				if (temp.contains("45"))
					team1++;
			} else if (Integer.parseInt(temp) <= 45)
				team1++;
		}

		for (int i = 0; i < match.team2_scorers.size(); i++) {

			String temp = match.team2_scorers.get(i).action_time.replaceAll(
					"'", "").replaceAll(":", "");

			if (temp.contains("+")) {
				if (temp.contains("45"))
					team2++;
			} else if (Integer.parseInt(temp) <= 45)
				team2++;

		}

		match.team_1_hfs = team1;
		match.team_2_hfs = team2;

	}

	private void calculateTotalRedCard() {

		int team1 = 0;
		int team2 = 0;

		for (int i = 0; i < match.team1_players.size(); i++) {
			if (match.team1_players.get(i).card_value.equalsIgnoreCase("red")
					|| match.team1_players.get(i).card_value
							.equalsIgnoreCase("Two Yellow"))
				team1++;
		}

		for (int i = 0; i < match.team2_players.size(); i++) {
			if (match.team2_players.get(i).card_value.equalsIgnoreCase("red")
					|| match.team2_players.get(i).card_value
							.equalsIgnoreCase("Two Yellow"))
				team2++;
		}

		team1_totoal_redcard_txt.setText(team1 + "");
		team2_totoal_redcard_txt.setText(team2 + "");

		if (team1 == 0)
			team1_totoal_redcard.setVisibility(RelativeLayout.GONE);

		if (team2 == 0)
			team2_totoal_redcard.setVisibility(RelativeLayout.GONE);
	}

	private void setData() {
		vs.setText(match.team_1 + " vs " + match.team_2);
		station.setText(match.station);

		/*
		 * if(match.isHalfTime.equalsIgnoreCase("Y")){ halftime.setText("HT " +
		 * match.team_1_score + " - " + match.team_2_score); }else{
		 * halftime.setText("HT 0 - 0"); }
		 */

		String half_prefix = "HT";
		/*
		 * if(match.isHalfTime.equalsIgnoreCase("Y")){ half_prefix = "HT"; }else
		 * if(match.isHalfTime.equalsIgnoreCase("N")){ half_prefix = "FT"; }
		 */

		if (isHalfTime()) {
			halftime.setText(half_prefix + " " + match.team_1_hfs + " - "
					+ match.team_2_hfs);
		} else {
			halftime.setText(half_prefix + " " + "0 - 0");
		}

		if (match.currentTimeStatus.equalsIgnoreCase("")) {

			try {
				Date d = old_df.parse(match.current_time);
				match.current_time = current_df.format(d);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			current_time.setText(match.current_time);

		} else {
			current_time.setText(match.currentTimeStatus);
		}

		team1_score.setText(match.team_1_score + "");
		team2_score.setText(match.team_2_score + "");

		if(match.team1_penaltyshoot == 0 && match.team2_penaltyshoot == 0){
			team1_pen.setVisibility(TextView.INVISIBLE);
			team2_pen.setVisibility(TextView.INVISIBLE);
			text_pen.setVisibility(TextView.INVISIBLE);
		}else{
			team1_pen.setVisibility(TextView.VISIBLE);
			team2_pen.setVisibility(TextView.VISIBLE);
			text_pen.setVisibility(TextView.VISIBLE);
		}
		
		team1_pen.setText(match.team1_penaltyshoot + "");
		team2_pen.setText(match.team2_penaltyshoot + "");

		calculateTotalRedCard();

		team_1.setText(match.team_1 + "");
		team_2.setText(match.team_2 + "");

		referee.setText("Ref : " + match.ref);

		L.i(match.current_time);

		team_1_scorer.removeAllViews();
		team_2_scorer.removeAllViews();

		for (int i = 0; i < match.team1_scorers.size(); i++) {
			ScorerItemView view = ScorerItemView_
					.build(getApplicationContext());
			view.bind(match.team1_scorers.get(i));
			team_1_scorer.addView(view);
		}

		for (int i = 0; i < match.team2_scorers.size(); i++) {
			ScorerItemView view = ScorerItemView_
					.build(getApplicationContext());
			view.bind(match.team2_scorers.get(i));
			team_2_scorer.addView(view);
		}

		ArrayList<Match> matches = new ArrayList<Match>();

		int size = 0;
		if (match.team1_players.size() > match.team2_players.size())
			size = match.team1_players.size();
		else
			size = match.team2_players.size();

		for (int i = 0; i < size; i++) {
			matches.add(match);
		}

		adapter.setArraylist(matches);

		list_player.setAdapter(adapter);
	}

	@OptionsItem(android.R.id.home)
	void home() {
		finish();
	}
}
