package com.starting11.android.ui;

import java.util.ArrayList;

import android.content.Intent;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.googlecode.androidannotations.annotations.AfterViews;
import com.googlecode.androidannotations.annotations.Bean;
import com.googlecode.androidannotations.annotations.Click;
import com.googlecode.androidannotations.annotations.EActivity;
import com.googlecode.androidannotations.annotations.Extra;
import com.googlecode.androidannotations.annotations.ItemClick;
import com.googlecode.androidannotations.annotations.OptionsItem;
import com.googlecode.androidannotations.annotations.OptionsMenu;
import com.googlecode.androidannotations.annotations.ViewById;
import com.googlecode.androidannotations.annotations.res.StringRes;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.starting11.android.R;
import com.starting11.android.Starting11App;
import com.starting11.android.adapter.MatchAdapter;
import com.starting11.android.core.BaseActivity;
import com.starting11.android.exception.ReturnException;
import com.starting11.android.object.Country;
import com.starting11.android.object.Match;
import com.starting11.android.utils.L;
import com.tonicartos.widget.stickygridheaders.StickyGridHeadersGridView;

@EActivity(R.layout.activity_match_country)
@OptionsMenu(R.menu.menu_refresh)
public class MatchesByCountry extends BaseActivity {

	@ViewById
	StickyGridHeadersGridView list_match;

	@ViewById
	ImageView loading;

	@Bean
	MatchAdapter adapter;

	ArrayList<Match> matches = new ArrayList<Match>();

	@Extra("country")
	Country country;

	@StringRes(R.string.url_matches)
	String url;

	boolean isLoading;

	@AfterViews
	public void calledAfterViewInjection() {

		getSupportActionBar().setTitle(country.name);
		getSupportActionBar().setIcon(country.icon);

		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setHomeButtonEnabled(true);

		adapter.setArraylist(matches);
		list_match.setAdapter(adapter);
		list_match.setNumColumns(1);

		getMatch();
	}

	@Click(R.id.loading)
	@OptionsItem(R.id.action_refresh)
	void OnRefresh(){
		if(!isLoading) getMatch();
	}

	@ItemClick(R.id.list_match)
	void OnItemClick(int position){

		if(!matches.get(position).lineup_ready.equalsIgnoreCase("N")){
			Intent detail = new Intent(getApplicationContext(), MatchDetail_.class);
			detail.putExtra("country", country);
			detail.putExtra("match", matches.get(position));
			startActivity(detail);
		}else{
			Toast.makeText(getApplicationContext(), "Line Up not ready", Toast.LENGTH_SHORT).show();
		}
	}

	private void getMatch(){

		isLoading = true;

		list_match.setVisibility(ListView.GONE);

		loading.setVisibility(ImageView.VISIBLE);
		loading.startAnimation(AnimationUtils.loadAnimation(mContext, R.anim.rotate_indefinitely));

		if(Starting11App.hasInternet(mContext)){
			Ion
			.with(mContext)
			.load(url)
			.setBodyParameter("country", country.id)
			.setBodyParameter("key", pref.key().get())
			.asString()
			.setCallback(new FutureCallback<String>() {	
				@Override
				public void onCompleted(Exception e, String result) {

					try {
						if (e != null) throw e;

						parseMatches(result);

						adapter.notifyDataSetChanged();

						isLoading = false;

					}
					catch (Exception ex) {
						ex.printStackTrace();

						isLoading = false;

						Toast.makeText(mContext, "Unable to connect", Toast.LENGTH_SHORT).show();
					}

					if(matches.isEmpty()){
						L.i("Arraylist is Empty");
						if(!isLoading){
							L.i("Not Loading");
							loading.setVisibility(ImageView.VISIBLE);
							loading.setAnimation(null);
						}
					}else{
						L.i("Arraylist is not Empty");

						list_match.setVisibility(ListView.VISIBLE);

						loading.setVisibility(ImageView.GONE);
						loading.setAnimation(null);
					}
				}

			});
		}else{

			isLoading = false;

			L.i("No Connection");

			Toast.makeText(mContext, "No Active Internet Connection", Toast.LENGTH_SHORT).show();
		}

	}

	private void parseMatches(String input) throws ReturnException{

		JsonParser parser = new JsonParser();

		JsonObject jobj_main = (JsonObject) parser.parse(input.trim());

		int status = jobj_main.get("status").getAsInt();

		L.i("Status : " + status);

		Gson gson = new GsonBuilder().create();

		matches.clear();

		if(status == 2){

			JsonArray jarr_items = jobj_main.getAsJsonArray("items");

			for(int i=0;i<jarr_items.size();i++){
				JsonObject jobj_items = jarr_items.get(i).getAsJsonObject();

				if(jobj_items.has("current")){
					JsonArray jarr_current = jobj_items.getAsJsonArray("current");

					for(int c=0;c<jarr_current.size();c++){
						Match match = gson.fromJson(jarr_current.get(c), Match.class);
						match.type = "current";
						match.currentTimeStatus = ((JsonObject) jarr_current.get(c)).get("currentTimeStatus").getAsString();
						matches.add(match);
					}
				}

				if(jobj_items.has("upcoming")){
					JsonArray jarr_upcoming = jobj_items.getAsJsonArray("upcoming");

					for(int u=0;u<jarr_upcoming.size();u++){
						Match match = gson.fromJson(jarr_upcoming.get(u), Match.class);
						match.type = "upcoming";

						matches.add(match);
					}
				}

				if(jobj_items.has("past")){
					JsonArray jarr_past = jobj_items.getAsJsonArray("past");

					for(int p=0;p<jarr_past.size();p++){
						Match match = gson.fromJson(jarr_past.get(p), Match.class);
						match.type = "past";

						matches.add(match);
					}
				}
			}

		}else{
			throw new ReturnException("Server Return is not 1");
		}

		isLoading = false;

		loading.setVisibility(ImageView.GONE);
	}

	@OptionsItem(android.R.id.home)
	void home(){
		finish();
	}
}
