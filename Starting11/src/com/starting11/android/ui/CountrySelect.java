package com.starting11.android.ui;

import java.util.ArrayList;

import android.content.Intent;
import android.graphics.Color;
import android.widget.ListView;

import com.googlecode.androidannotations.annotations.AfterViews;
import com.googlecode.androidannotations.annotations.Bean;
import com.googlecode.androidannotations.annotations.EActivity;
import com.googlecode.androidannotations.annotations.ItemClick;
import com.googlecode.androidannotations.annotations.OptionsItem;
import com.googlecode.androidannotations.annotations.OptionsMenu;
import com.googlecode.androidannotations.annotations.ViewById;
import com.starting11.android.R;
import com.starting11.android.adapter.CountryAdapter;
import com.starting11.android.core.BaseActivity;
import com.starting11.android.object.Country;

@EActivity(R.layout.activity_country_select)
@OptionsMenu(R.menu.menu_about)
public class CountrySelect extends BaseActivity {

	@ViewById
	ListView list_country;

	@Bean
	CountryAdapter adapter;

	ArrayList<Country> countries = new ArrayList<Country>();

	@AfterViews
	public void calledAfterViewInjection() {

		populateCountry();

		adapter.setArraylist(countries);
		list_country.setAdapter(adapter);

	}
	
	@OptionsItem(R.id.action_about)
	void AboutUs(){
		Intent about = new Intent(getApplicationContext(), About_.class);
		startActivity(about);
	}

	@ItemClick(R.id.list_country)
	void OnItemClick(int position){
		Intent matches = new Intent(getApplicationContext(), MatchesByCountry_.class);
		matches.putExtra("country", countries.get(position));
		startActivity(matches);
	}

	private void populateCountry(){

		Country england = new Country();
		england.id = "england";
		england.name = "England";
		england.icon = R.drawable.england;
		england.bg = Color.parseColor("#3B3B3B");

		Country italy = new Country();
		italy.id = "italy";
		italy.name = "Italy";
		italy.icon = R.drawable.italy;
		italy.bg = Color.parseColor("#303030");

		Country germany = new Country();
		germany.id = "germany";
		germany.name = "Germany";
		germany.icon = R.drawable.germany;
		germany.bg = Color.parseColor("#3B3B3B");

		Country spain = new Country();
		spain.id = "spain";
		spain.name = "Spain";
		spain.icon = R.drawable.spain;
		spain.bg = Color.parseColor("#303030");

		Country france = new Country();
		france.id = "france";
		france.name = "France";
		france.icon = R.drawable.france;
		france.bg = Color.parseColor("#3B3B3B");

		Country champion = new Country();
		champion.id = "champion";
		champion.name = "Champions League";
		champion.icon = R.drawable.uefa;
		champion.bg = Color.parseColor("#303030");

		Country europa = new Country();
		europa.id = "europa";
		europa.name = "Europa League";
		europa.icon = R.drawable.europa;
		europa.bg = Color.parseColor("#3B3B3B");

		Country international = new Country();
		international.id = "international";
		international.name = "International";
		international.icon = R.drawable.international;
		international.bg = Color.parseColor("#303030");
		
		Country worldcup = new Country();
		worldcup.id = "worldcup2014";
		worldcup.name = "World Cup 2014";
		worldcup.icon = R.drawable.worldcup;
		worldcup.bg = Color.parseColor("#303030");

		countries.add(worldcup);
		countries.add(england);
		countries.add(italy);
		countries.add(germany);
		countries.add(spain);
		countries.add(france);
		countries.add(champion);
		countries.add(europa);
		countries.add(international);

	}
}
