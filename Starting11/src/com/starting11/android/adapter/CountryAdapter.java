package com.starting11.android.adapter;

import android.view.View;
import android.view.ViewGroup;

import com.googlecode.androidannotations.annotations.EBean;
import com.starting11.android.core.BaseAdapter;
import com.starting11.android.object.Country;
import com.starting11.android.view.CountryItemView;
import com.starting11.android.view.CountryItemView_;

@EBean
public class CountryAdapter extends BaseAdapter<Country> {

	public CountryAdapter() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		CountryItemView view;
		if (convertView == null) {
			view = CountryItemView_.build(mContext);
		} else {
			view = (CountryItemView) convertView;
		}

		view.bind((Country) getItem(position));

		return view;
	}

}
