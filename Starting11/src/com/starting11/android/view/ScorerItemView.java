package com.starting11.android.view;

import android.content.Context;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.googlecode.androidannotations.annotations.EViewGroup;
import com.googlecode.androidannotations.annotations.ViewById;
import com.starting11.android.R;
import com.starting11.android.object.Player;

@EViewGroup(R.layout.listitem_scorer)
public class ScorerItemView extends LinearLayout{

	@ViewById
	TextView scorer;
	
	@ViewById
	ImageView goal_type;
	
	public ScorerItemView(Context context) {
		super(context);
	}
	
	public void bind(Player s) {
		scorer.setText(s.name + " " + s.action_time);
		
		if(s.goal_type.equalsIgnoreCase("Normal Goal")){
			goal_type.setVisibility(ImageView.GONE);
		}else if(s.goal_type.equalsIgnoreCase("Penalty Goal")){
			goal_type.setImageResource(R.drawable.pk);
		}else if(s.goal_type.equalsIgnoreCase("Own Goal")){
			goal_type.setImageResource(R.drawable.og);
		}else if(s.goal_type.equalsIgnoreCase("miss")){
			goal_type.setImageResource(R.drawable.miss);
		}
	}

}
