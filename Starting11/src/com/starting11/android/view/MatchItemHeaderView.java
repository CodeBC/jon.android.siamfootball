package com.starting11.android.view;

import android.content.Context;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.googlecode.androidannotations.annotations.EViewGroup;
import com.googlecode.androidannotations.annotations.ViewById;
import com.starting11.android.R;

@EViewGroup(R.layout.listitem_header)
public class MatchItemHeaderView extends LinearLayout{

	@ViewById
	TextView header;

	public MatchItemHeaderView(Context context) {
		super(context);
	}

	public void bind(String name) {
		
		if(name.equalsIgnoreCase("current")){
			header.setText("Current Matches");
		}else if(name.equalsIgnoreCase("upcoming")){
			header.setText("Upcoming Matches");
		}else if(name.equalsIgnoreCase("past")){
			header.setText("Past Matches");			
		}
		
	}

}
