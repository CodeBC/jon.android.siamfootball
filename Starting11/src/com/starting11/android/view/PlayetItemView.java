package com.starting11.android.view;

import android.content.Context;
import android.graphics.Color;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.googlecode.androidannotations.annotations.EViewGroup;
import com.googlecode.androidannotations.annotations.ViewById;
import com.starting11.android.R;
import com.starting11.android.object.Player;
import com.starting11.android.utils.L;

@EViewGroup(R.layout.listitem_player)
public class PlayetItemView extends LinearLayout{

	@ViewById
	LinearLayout team_1;

	@ViewById
	TextView team_1_player;

	@ViewById
	TextView team_1_player_no;

	@ViewById
	ImageView team_1_player_card;

	@ViewById
	RelativeLayout team_1_player_inout;
	
	@ViewById
	ImageView team_1_player_inout_img;

	@ViewById
	TextView team_1_player_inout_txt;

	@ViewById
	TextView type;

	@ViewById
	LinearLayout team_2;

	@ViewById
	TextView team_2_player;

	@ViewById
	TextView team_2_player_no;

	@ViewById
	ImageView team_2_player_card;

	@ViewById
	RelativeLayout team_2_player_inout;

	@ViewById
	TextView team_2_player_inout_txt;

	@ViewById
	ImageView team_2_player_inout_img;
	
	public PlayetItemView(Context context) {
		super(context);
	}

	public void bind(Player p1,Player p2,boolean isSeperator) {

		if(isSeperator){
			team_1.setBackgroundColor(Color.parseColor("#6a6a6a"));
			team_2.setBackgroundColor(Color.parseColor("#6a6a6a"));
			type.setBackgroundColor(Color.parseColor("#6a6a6a"));

			type.setVisibility(TextView.VISIBLE);

			type.setText("RES");

			team_1_player.setVisibility(TextView.GONE);
			team_1_player_no.setVisibility(TextView.GONE);
			team_1_player_card.setVisibility(ImageView.GONE);
			team_1_player_inout.setVisibility(ImageView.GONE);
			team_2_player.setVisibility(TextView.GONE);
			team_2_player_no.setVisibility(TextView.GONE);
			team_2_player_card.setVisibility(ImageView.GONE);
			team_2_player_inout.setVisibility(ImageView.GONE);
		}else{

			//Reset View
			team_1_player.setTextColor(Color.WHITE);
			team_2_player.setTextColor(Color.WHITE);
			team_1.setBackgroundColor(Color.parseColor("#333333"));
			team_2.setBackgroundColor(Color.parseColor("#333333"));

			team_1_player.setVisibility(TextView.VISIBLE);
			team_1_player_card.setVisibility(ImageView.VISIBLE);
			team_1_player_inout.setVisibility(ImageView.VISIBLE);
			team_1_player_no.setVisibility(TextView.VISIBLE);
			team_2_player.setVisibility(TextView.VISIBLE);
			team_2_player_card.setVisibility(ImageView.VISIBLE);
			team_2_player_inout.setVisibility(ImageView.VISIBLE);
			team_2_player_no.setVisibility(TextView.VISIBLE);
			type.setVisibility(TextView.GONE);
			//Reset Finish

			//TEAM 1
			if(p1 != null && p1.name != null){
				team_1_player_no.setText(p1.number);
				team_1_player.setText(p1.name);

				L.i("Team 1 Player : " + p1.name);
				
				if(p1.card_value.equalsIgnoreCase("red")){
					team_1_player_card.setVisibility(ImageView.VISIBLE);
					team_1_player_card.setImageResource(R.drawable.redcard);
					team_1_player.setTextColor(Color.parseColor("#5d5c5d"));
				}else if(p1.card_value.equalsIgnoreCase("two yellow")){
					team_1_player_card.setVisibility(ImageView.VISIBLE);
					team_1_player_card.setImageResource(R.drawable.two_yellow);
					team_1_player.setTextColor(Color.parseColor("#5d5c5d"));
				}else if(p1.card_value.equalsIgnoreCase("yellow")){ 
					team_1_player_card.setVisibility(ImageView.VISIBLE);
					team_1_player_card.setImageResource(R.drawable.yellowcard);
				}else{
					team_1_player_card.setVisibility(ImageView.GONE);
				}

				if(p1.isReplace.equalsIgnoreCase("y")){
					team_1_player_inout.setVisibility(RelativeLayout.VISIBLE);
					team_1_player_inout_txt.setText(p1.replaced_time);
					team_1_player.setTextColor(Color.parseColor("#1b6700"));
					
					if(p1.type.equalsIgnoreCase("RS"))
						team_1_player.setTextColor(Color.RED);
					else
						team_1_player.setTextColor(Color.parseColor("#1b6700"));
					
				}else{
					team_1_player_inout.setVisibility(RelativeLayout.GONE);
				}
				
				if(p1.type.equalsIgnoreCase("RS")){
					team_1_player_inout_img.setImageResource(R.drawable.redinout);
				}else{
					team_1_player_inout_img.setImageResource(R.drawable.inout);
				}
			}


			//TEAM 2
			if(p2 != null && p2.name != null){
				team_2_player_no.setText(p2.number);
				team_2_player.setText(p2.name);

				L.i("Team 2 Player : " + p2.name);

				if(p2.card_value.equalsIgnoreCase("red")){
					team_2_player_card.setVisibility(ImageView.VISIBLE);
					team_2_player_card.setImageResource(R.drawable.redcard);
					team_2_player.setTextColor(Color.parseColor("#5d5c5d"));
				}else if(p2.card_value.equalsIgnoreCase("two yellow")){
					team_2_player_card.setVisibility(ImageView.VISIBLE);
					team_2_player_card.setImageResource(R.drawable.two_yellow);
					team_2_player.setTextColor(Color.parseColor("#5d5c5d"));
				}else if(p2.card_value.equalsIgnoreCase("yellow")){ 
					team_2_player_card.setVisibility(ImageView.VISIBLE);
					team_2_player_card.setImageResource(R.drawable.yellowcard);
				}else{
					team_2_player_card.setVisibility(ImageView.GONE);
				}

				if(p2.isReplace.equalsIgnoreCase("y")){
					team_2_player_inout.setVisibility(RelativeLayout.VISIBLE);
					team_2_player_inout_txt.setText(p2.replaced_time);
					
					if(p2.type.equalsIgnoreCase("RS"))
						team_2_player.setTextColor(Color.RED);
					else
						team_2_player.setTextColor(Color.parseColor("#1b6700"));
				}else{
					team_2_player_inout.setVisibility(RelativeLayout.GONE);
				}	
				
				if(p2.type.equalsIgnoreCase("RS")){
					team_2_player_inout_img.setImageResource(R.drawable.redinout);
				}else{
					team_2_player_inout_img.setImageResource(R.drawable.inout);
				}
			}

		}
	}

}
