package com.starting11.android;

import android.app.Application;
import android.content.Context;
import android.net.ConnectivityManager;
import android.util.Log;

import com.googlecode.androidannotations.annotations.EApplication;
import com.koushikdutta.ion.Ion;

/**
 * Main Application
 *
 * @author Hein Win Toe (hein[at]nexlabs[dot]co)
 * @since 1.0.0
 */

@EApplication
public class Starting11App extends Application {

	@Override
	public void onCreate() {
		super.onCreate();
		
		Ion.getDefault(this).setLogging("Starting11", Log.DEBUG);
	}

	public static boolean hasInternet(Context context) {
		ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
		return cm.getActiveNetworkInfo() != null;
	}

}
